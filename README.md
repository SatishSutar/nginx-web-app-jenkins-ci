# DevOps Project

## Title

Launching NGINX Web Server using Jenkins CI

![Architecture diagram](https://drive.google.com/uc?id=1ld_5tpknv2II4Mv11yT1UbE0PB0-Ws59)

## Description

This is a simple project that automates the build, test, and deployment process of a basic NGINX web server using Jenkins as a CI tool. 

The project involves configuring the web server on an EC2 instance, modifying the web pages using VS Code, and pushing the changes to the version control system. 

## Automation

Jenkins is used to automate the build process and deploy the changes to the web server.

The Jenkins pipeline is used to define the stages of the build process, including:

- checkout,
- build,
- test, and 
- deployment. 

The pipeline is triggered by a manual job, and it executes the stages in sequence, providing feedback and notifications to the user. The project demonstrates how Jenkins can be used to streamline the development and deployment process, reducing errors and increasing efficiency.

## Disclaimer

Although this is a basic DevOps project, it represents a significant achievement in configuring a web server and automating the build and deployment process using Jenkins. I believe in celebrating small successes and recognizing the effort that goes into every project.


